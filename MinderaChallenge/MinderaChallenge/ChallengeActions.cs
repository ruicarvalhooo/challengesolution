﻿using OpenQA.Selenium.Interactions;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;
using System;

namespace MinderaChallenge
{
    public class ChallengeActions
    {
        private ChallengePage challengePage;
        private IWebDriver driver;

        public ChallengeActions(IWebDriver driver)
        {
            this.driver = driver;
            challengePage = new ChallengePage(this.driver);
        }


        #region Test methods

        /// <summary>
        /// Sorts the rows by the number on their label by dragging and dropping them
        /// </summary>
        /// <returns>True if the rows were sorted</returns>
        public bool SortListRows()
        {
            bool result = false;
            List<string> positionsToTarget = GetTargetPositioning();
            int listLength = challengePage.SortableRows.Count;

            try
            {
                for (int positionToFillIn = 0; positionToFillIn < listLength; positionToFillIn++)
                    //Look for the number that the row of index 'positionToFillIn' should display when sorted
                    for (int rowsIterator = positionToFillIn; rowsIterator < listLength; rowsIterator++)
                        if (challengePage.SortableRows[rowsIterator].Text.Split(' ')[1] == positionsToTarget[positionToFillIn])
                        {
                            if (positionToFillIn == rowsIterator) //break if the number already is on the target position
                                break;
                            else
                            {
                                if (!DragDropRow(rowsIterator, positionToFillIn))
                                    throw new Exception("Could not drag/drop row");
                                break;
                            }
                        }

                result = GetCurrentPositioning().SequenceEqual(positionsToTarget);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return result;
        }

        /// <summary>
        /// Drags a row according to the positions (indexes) passed as argument
        /// </summary>
        /// <param name="from">Index of the row to be moved</param>
        /// <param name="to">Index of the position to where the row should be moved</param>
        /// <returns>True if the row was dragged and dropped to the required position</returns>
        public bool DragDropRow(int from, int to)
        {
            bool result = false;

            try
            {
                string fromRowLabel = challengePage.SortableRows[from].Text;

                Actions action = new Actions(this.driver);
                action.DragAndDrop(challengePage.SortableRows[from], challengePage.SortableRows[to]).Build().Perform();
                challengePage.SortableRows[to].Click(); //To force finishing the drop action

                //Verify that the position to where the row was moved contains that row's label 
                result = fromRowLabel == challengePage.SortableRows[to].Text;
            }
            catch (Exception e)
            {
            }

            return result;
        }

        /// <summary>
        /// Check if the second word of each rows' labels is numeric
        /// </summary>
        /// <returns>True if all rows contain a numeric value on the defined position</returns>
        public bool AllRowsAreProperlyNumbered()
        {
            bool result = true;

            try
            {
                foreach(IWebElement row in challengePage.SortableRows)
                    if(!double.TryParse(row.Text.Split(' ')[1], out double number))
                    {
                        result = false;
                        break;
                    }
            }
            catch (Exception e)
            {
                result = false;
            }

            return result;
        }

        #endregion Test methods


        #region Support methods

        /// <summary>
        /// Auxiliar method that returns a "List<string>" containing the rows' numbers in the exact same order they are positioned on list
        /// </summary>
        /// <returns>A list of strings (List<string>), as described on summary</returns>
        private List<string> GetCurrentPositioning()
        {
            List<string> rowsLabels = new List<string>();

            try
            {
                //foreach (IWebElement sortableRow in sortableList)
                //    rowsLabels.Add(sortableRow.Text);

                foreach (IWebElement sortableRow in challengePage.SortableRows)
                    rowsLabels.Add(sortableRow.Text.Split(' ')[1]);
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not parse each of the rows' labels. ");
                return null;
            }

            return rowsLabels;
        }


        /// <summary>
        /// Auxiliar method that returns a list containing the list rows' numbers sorted ascendently
        /// </summary>
        /// <returns>A list of strings (List<string>), as described on summary</returns>
        private List<string> GetTargetPositioning()
        {
            List<string> targetPositions = new List<string>();

            try
            {
                targetPositions = GetCurrentPositioning();
                targetPositions.Sort();
            }
            catch (Exception e)
            {
                return null;
            }

            return targetPositions;
        }

        #endregion Support methods

    }

}
