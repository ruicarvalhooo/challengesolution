﻿using NUnit.Framework;
using NUnitLite;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using System;

namespace MinderaChallenge
{

    [TestFixture("chrome")]
    public class ChallengeTests
    {
        private string browser;

        private ChallengeActions challengeActions;
        private ChallengePage challengePage;

        private WebDriverWait wait;
        private IWebDriver driver;

        public ChallengeTests(string browser)
        {
            this.browser = browser;
        }

        //To run using NunitLite
        public static int Main(string[] args)
        {
            return new AutoRun().Execute(args);
        }


        #region Setup

        [OneTimeSetUp]
        public void Initialize()
        {
            try
            {
                if (browser == "chrome")
                {
                    driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub/"), new ChromeOptions());
                    driver.Navigate().GoToUrl("webappcontainer:3000");

                    //Instantiate the 'Actions' and 'Page' classes passing the initialized WebDriver instance
                    challengeActions = new ChallengeActions(driver);
                    challengePage = new ChallengePage(driver);
                }
                else
                    throw new PlatformNotSupportedException("Only Chrome driver is supported");
            }
            catch (Exception e)
            {
                throw new InconclusiveException("Could not initialize driver or navigate to the defined 'Url'. " + e.Message);
            }
        }


        [SetUp]
        public void SetupBeforeTest()
        {
            //Make sure that the application is on the required state between each test execution
            try
            {
                wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                wait.Until(d => challengePage.SortableRowsContainer.Displayed && challengePage.SortableRowsContainer.Enabled);
            }
            catch (WebDriverTimeoutException)
            {
                throw new InconclusiveException("Could not reach the required initial state");
            }
        }


        [OneTimeTearDown]
        public void Cleanup()
        {
            if (driver != null)
                driver.Quit();
        }

        #endregion Setup



        #region Tests

        [Test]
        [Description("Verify that all rows are properly numbered")]
        public void AllRowsAreNumbered()
        {
            Assert.That(challengeActions.AllRowsAreProperlyNumbered(), Is.True, "Rows aren't all properly numbered");
        }

        [Test]
        [Description("Validate that the list's rows are draggable")]
        public void RowsAreDraggable()
        {
            //These variables should not be hardcoded
            int from = 1;
            int to = 2;

            Assert.That(challengeActions.DragDropRow(from, to), Is.True, "Could not drag row");
        }

        [Test]
        [Description("Verify that the list is - and can be - sorted")]
        public void SortRows()
        {
            Assert.That(challengeActions.SortListRows(), Is.True, "Could not sort rows");
        }

        #endregion Tests

    }
}