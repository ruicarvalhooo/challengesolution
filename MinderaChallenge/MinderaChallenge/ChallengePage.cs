﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace MinderaChallenge
{
    public class ChallengePage
    {
        //In case of selector updates, we would only have to fix it once 
        private const string ROWS_CONTAINER_CSS_SELECTOR = "#app > ul";

        public ChallengePage(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }



        /// <summary>
        /// "IWebElement" representing the draggable rows' container 
        /// </summary>
        [FindsBy(How = How.CssSelector, Using = ROWS_CONTAINER_CSS_SELECTOR)]
        public IWebElement SortableRowsContainer { get; set; }


        /// <summary>
        /// "IList<IWebElement>" representing the draggable rows
        /// </summary>
        [FindsBy(How = How.CssSelector, Using = ROWS_CONTAINER_CSS_SELECTOR + " li")]
        public IList<IWebElement> SortableRows { get; set; }
    }
}
