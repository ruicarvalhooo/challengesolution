
## Prerequisites

**Minimum requirements:** Docker; Windows  
**Additional requirements:** Visual Studio; VNC Viewer

---

## Step 1: Build and run Docker containers

Before running the tests, we need to build and start some required Docker containers.  
To do so, and assuming you already have Docker installed on your machine, you just have to:

1. Open the Shell/Command Prompt and navigate to the directory where the 'docker-compose.yml' file is located;
2. Type "docker-compose up -d" and wait for the containers to be build.
  
Now we have two containers running (one hosting the Challenge Web App, and the other containing the Selenium environment).

---

## Step 2: Run the tests

To run the tests, we have two options:  

**Option 1:**	Go to the "Executables" folder and run the "MinderaChallenge.exe" file.

The tests will run in the created Selenium Docker container. After having finished, NunitLite will create a "TestResult.xml" report on the same folder.  

**Option 2:**	Open the solution on Visual Studio and select the tests you want to run (on 'Test Explorer').

***Suggestion**:	If you want to see the tests running and/or debug tests, install "VNC Viewer" and connect to "http://localhost:5900"*